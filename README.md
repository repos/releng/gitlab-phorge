# gitlab-phorge

Provides Phorge integration with GitLab CE. For now, this is simply the
rendering of task numbers as links to their Phorge task URLs.

## Installation

This integration is packaged as a Ruby Gem, specifically a Rails Engine. It
must be sided loaded with the GitLab CE Rails application so it can "register"
the integration class early.

## License

gitlab-phorge is licensed under the GNU General Public License 3.0 or later
(GPL-3.0+). See the LICENSE file for more details.
