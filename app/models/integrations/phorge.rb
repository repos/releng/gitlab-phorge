# frozen_string_literal: true

module Integrations
  class Phorge < BaseIssueTracker
    include Integrations::HasIssueTrackerFields

    validates :project_url, :issues_url, :new_issue_url, presence: true, public_url: true, if: :activated?

    def title
      'Phorge'
    end

    def description
      s_("IssueTracker|Use Phorge as this project's issue tracker.")
    end

    def help
      docs_link = ActionController::Base.helpers.link_to _('Learn more.'), 'https://gitlab.wikimedia.org/repos/releng/gitlab-phorge', target: '_blank', rel: 'noopener noreferrer'
      s_('IssueTracker|Use Phorge as the issue tracker. %{docs_link}').html_safe % { docs_link: docs_link.html_safe }
    end

    def reference_pattern(only_long: false)
      @reference_pattern ||= /\b(?<issue>T\d+)(?=\W|\z)/
    end

    def self.to_param
      'phorge'
    end
  end
end
