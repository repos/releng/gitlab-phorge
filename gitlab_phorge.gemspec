require_relative "lib/gitlab_phorge/version"

Gem::Specification.new do |spec|
  spec.name        = "gitlab_phorge"
  spec.version     = GitlabPhorge::VERSION
  spec.authors     = ["Wikimedia Release Engineering Team"]
  spec.email       = ["Wikimedia Release Engineering Team <releng@lists.wikimedia.org>"]
  spec.homepage    = "https://gitlab.wikimedia.org/repos/releng/gitlab-phorge"
  spec.summary     = "Phorge integration for GitLab CE"
  spec.description = "Integrates Phorge with GitLab CE"
  spec.license     = "GPL-3.0-or-later"

  spec.metadata["homepage_uri"] = spec.homepage

  spec.files = Dir["{app,config,db,lib}/**/*", "LICENSE", "README.md"]

  spec.add_dependency "rails", ">= 6.1"
end
