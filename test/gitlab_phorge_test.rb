require "test_helper"

class GitlabPhorgeTest < ActiveSupport::TestCase
  test "it has a version number" do
    assert GitlabPhorge::VERSION
  end

  test "it has been added 'phorge' to Integration::INTEGRATION_NAMES" do
    assert Integration::INTEGRATION_NAMES.include?("phorge")
  end
end
