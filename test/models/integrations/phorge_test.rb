require "test_helper"

module Integrations
  class PhorgeTest < ActiveSupport::TestCase
    test "reference pattern finds task references" do
      phorge = Integrations::Phorge.new

      assert phorge.reference_pattern.match(<<~end)[:issue] == "T123"
        Description that references T123 something something
      end
    end
  end
end
